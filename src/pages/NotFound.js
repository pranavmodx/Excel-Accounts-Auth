import React from 'react';

const NotFound = () => {
    return (
        <div className='fullCenter'>
            404 Not Found
        </div>
    );
}

export default NotFound;